NetworkManager:
  pkg.installed:
    - pkgs:
      - networkmanager
      - openvpn
      - networkmanager-openvpn
  service:
    - running
    - enable: True
    - restart: True
    - requires:
      - pkg: networkmanager
