gnome:
  pkg.installed:
    - pkgs:
      # gnome
      - adwaita-icon-theme
      - baobab
      - dconf-editor
      - eog
      - evince
      - gnome-backgrounds
      - gnome-calculator
      - gnome-control-center
      - gnome-desktop
      - gnome-disk-utility
      - gnome-font-viewer
      - gnome-keyring
      - gnome-screenshot
      - gnome-session
      - gnome-settings-daemon # Conflict with gnome-settings-daemon-redshift
      - gnome-shell
      - gnome-shell-extensions
      - gnome-system-log
      - gnome-system-monitor
      - gnome-terminal
      - gnome-themes-extra
      - gnome-user-share
      - gucharmap
      - gvfs
      - gvfs-afc
      - gvfs-goa
      - gvfs-google
      - gvfs-gphoto2
      - gvfs-mtp
      - gvfs-nfs
      - gvfs-smb
      - mutter
      - nautilus
      - sushi
      - tracker
      - xdg-user-dirs-gtk
      # gnome-extra
      - file-roller
      - gnome-logs # gnome-system-log ?
      - gnome-tweaks

gdm.state:
  pkg.installed:
    - pkgs:
      - gdm
  service.running:
    - name: gdm
    - enable: True
    - requires:
      - pkg: gdm.state
