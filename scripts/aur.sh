#! /bin/sh
set -e

#
# Minimal AUR helper
#

cd /tmp
su -p nobody -c "curl https://aur.archlinux.org/cgit/aur.git/snapshot/${1}.tar.gz | tar xz"
cd ${1}
source ./PKGBUILD
if [ $(vercmp $pkgver $( pacman -Q ${1} | cut -d ' ' -f2 ) ) == 1 ]; then
  pacman -S --needed --noconfirm --asdeps ${depends[@]} ${makedepends[@]}
  if [ ! -z ${validpgpkeys} ]; then
    su -p nobody -c "HOME=/tmp/${1}; gpg --recv-keys ${validpgpkeys}; makepkg -c"
  else
    su -p nobody -c "makepkg -c"
  fi;
  pacman -U --needed --noconfirm *.pkg.tar.xz
  changed=true;
else
  echo "nothing to do. $1 is up to date $pkgver $(pacman -Q $1)"
  changed=false;
fi;

# writing the state line
echo  # an empty line here so the next line will be the last.
echo "changed=${changed}"
