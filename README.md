# Cellar

Cellar is a set of scripts to backup and setup machines.
It is roughly composed of 4 parts :
- Credentials (a private repository)
- [Sodium](https://gitlab.com/etnbrd/sodium)
- [Chloride](https://gitlab.com/etnbrd/chloride)
- [Settler](https://gitlab.com/etnbrd/settler)

Cellar is like a dotfile manager but for whole machines, using [SaltStack](https://saltstack.com/) (Sodium + Chloride = Salt).

## The story

I used to have a dotfile repo, like everybody else, with clever symlinks. I even implemented a filter mechanism to deploy dotfiles differently on different machines.
At the same time, I use Salt to provision and maintain my personal machines (in a masterless manner).
Salt is way more powerful than what I could ever implement in my dotfiles scripts.
So I switched from bash scripts to salt states for my dotfiles.
Here is the result.

## Example

You can see [this script](https://gitlab.com/etnbrd/settler/blob/master/deploy.sh) for a complete example of deploying the state on a base machine.

## The components

### [Sodium](https://gitlab.com/etnbrd/sodium)

**[Sodium](https://gitlab.com/etnbrd/sodium)** is the personalised part of salt. The sodium repo hosts my Sodium. You should host yours.

It contains all my dotfiles, and more (like locals, network configuration, sudoers files and so on) needed to transform an emtpy machine into the one I am accustomed to. It requires *Chloride*, and a another repo containing my credentials.

### [Chloride](https://gitlab.com/etnbrd/chloride)

**[Chloride](https://gitlab.com/etnbrd/chloride)** is the common part of salt.
It contains macros and formulas to ease the defintion of Sodium.
It intends to grow with time, and usage.

## Composition

### Folder structure

```
home/etn/.cellar
  | credentials
  ` salt
    | chloride
    | | formulas
    | ` macros
    ` sodium
      ` states
```

See belon for the structure of the credential folder

### Highstate

`salt-call --local state.highstate`

# Chloride

## Macros

Chloride provides a set of macros to helps defining salt states for your machines.

### User

#### users
`users(list)`
Apply the macro `user` for all user in `list`.

#### user
`user(name, params)`
Use the following pillar state to provision a user.
I also calls the macros `gnupg` and `ssh` with the params if the corresponding states are set for the user.

```
users:
  < username >:
    groups:
      - < some group >
    credentials:
      passwd: < you password, as appear in /etc/shadow >
      gnupg:
        present: true
      ssh:
        authorized_hosts:
          - <key>
        known_hosts:
          gitlab.com:
            enc: ssh-rsa
            key: <key>
          github.com:
            enc: ssh-rsa
            key: <key>

```

#### gnupg
`gnupg(user, params)`
Symlink the `.gnupg` repository from the `user`'s home folder to the `gnupg` folder in the `user`'s credentials folder.
See above for the definition of `params`.

#### ssh
`ssh(user, params)`
Symlink the `.ssh` repository from the `user`'s home folder to the `ssh` folder in the `user`'s credentials folder.
See above for the definition of `params`.

### General

#### symlink
`symlink(user, group, mode, source, target)`
Create a symlink from `source` to `target`.
`target` should be in your sodium repository, `source` should be in your home folder, in your etc folder, or wherever. The source will have `user`:`group` ownership, and `mode`.

#### credentials
`credentials(source, target, user, mode)``
Create a symlink from `source` to `target` in your credentials folder.
`user`, and `mode` defaults respectively to  `root` and `700`.

#### dotfiles
`dotfile(user, source, target, mode)`
Create a symlink from the `source` in `user`'s home folder to `target` in `user`'s `sodium/states` folder.

### git

#### gitclone
`gitclone(user, url, path)`
Clone the repository from `url` to `path`, using as `user`.

### gnome

#### gsetting
`gsettings(user, path, key, value)`
Set the gsetting located at `path`/`key` to `value`, for `user`.

#### dconf
`dconf(file)`
Load the keys in `file` (from `sodium/states`) into the GSettings database

## Formulas

### gnome
```
include:
  - formulas.gnome
```
Install the basic `Gnome` packages, and assure the `gdm` service is enabled and running.

### NetworkManager
```
include:
  - formulas.NetworkManager
```
Install `NetworkManager` along with `openvpn`, and assure the service is enabled and running.


# Credentials

## Folder structure

```
home/user/.cellar
  | credentials
  | | gnupg
  | | ssh
  | ` pillar
  |   | minion.sls -> link to /home/user/salt/states/sodium/states/base/minion
  |   | user.sls -> see above for content
  |   | top.sls -> include user and minion
  ` salt
    | chloride
    | | formulas
    | ` macros
    ` sodium
      ` states
```


## How to set up a credential repo

### Install
install `git-remote-gcrypt`

### Init repo

`git init`
create private repo on gitlab
`git remote add gitlab gcrypt::<your repo url>`

### Commit & Push

```
git add --all
git commit -m 'init'
git push -u gitlab master
```

Don't forget to unprotect the master branch on gitlab after the first push, otherwise, the next push will fail.

### Clone the repository on a new machine

Setup your ssh and gnupg credentials.

If you have a prompt :
```
git clone gcrypt::<your repo url>
```

If you want to automate the deployement :
```
git clone --config gcrypt.gpg-args="--batch --pinentry-mode loopback --passphrase ${passphrase}" gcrypt::git@gitlab.com:etnbrd/credentials.git;
```
